// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
// "Last car is a *car make goes here* *car model goes here*"


/**
 * Gets the details of the last car in the inventory
 * @param  {Object[]} inventory Inventory of cars in the dealership
 * @return {Array} car make and model of the last car 
 */
 function problem2(inventory) {
    
    // return empty object if empty or invalid inventory data type 
    if (  !Array.isArray(inventory) || inventory.length === 0) {
        return [];
    }
    // get the car which is stored in the end
    let lastCar = inventory[inventory.length - 1];
    
    return [lastCar.car_make, lastCar.car_model];
}

module.exports = problem2;