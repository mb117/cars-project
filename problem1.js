// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"


/**
 * Gets the details of the car with a specific ID
 * @param  {Object[]} inventory Inventory of cars in the dealership
 * @return {number} id id of the specific car
 * @return {Object} details of the car with the specified id
 */
 function problem1(inventory, id) {

    // return empty object if empty or invalid inventory data type
    if ( !Array.isArray(inventory) || typeof id !== 'number' || inventory.length === 0) {
        return [];
    }
    for ( let car of inventory) {
        if ( typeof car !== 'object') {
            return [];
        }
        if ( car.id === id) {
            return car;
        }
    }

    // found no match
    return [];
}


module.exports = problem1;