// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


/**
 * Gets the details of the car with a specific ID
 * @param  {Object[]} inventory Inventory of cars in the dealership
 * @param  {Array}  preferredMake names of the car make to see
 * @return {Array} cars containing the specified make
 */
 function problem6(inventory, preferredMake) {
    // return empty object if empty or invalid data 
    if (  !Array.isArray(inventory) || inventory.length === 0 || typeof preferredMake !== 'object' || preferredMake.length === 0) {
        return [];
    }

    let preferredCars = [];

    for ( let car of inventory) {
        // car should be an object
        if ( typeof car !== 'object') {
            return [];
        }
        // check if car is in the preferred make
        if ( preferredMake.indexOf(car.car_make) > -1) {
            preferredCars.push(car);
        }
    }

    return preferredCars;
}


module.exports = problem6;