// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


/**
 * Gets the details of the car with a specific ID
 * @param  {Object[]} inventory Inventory of cars in the dealership
 * @param  {Array} dates all the car dates in the inventory
 * @param  {Number} year cars made before this year
 * @return {Array} cars older than the specified year
 */
 function problem5(inventory, dates, year) {
    // return empty object if empty or invalid data 
    if (  !Array.isArray(inventory) || inventory.length === 0 || typeof dates !== 'object' || typeof year !== 'number') {
        return [];
    }
    let oldCarsCounter = 0;
    let oldCarsDetails = [];

    for ( let date of dates) {
        if ( date < year) {
            oldCarsCounter++;
        }
    }
    console.log(`The number of cars older than the year ${year} is ${oldCarsCounter}`);

    for ( let car of inventory) {
        // car should be an object
        if ( typeof car !== 'object') {
            return [];
        }
        if ( car.car_year < year) {
            oldCarsDetails.push(car);
        }
    }

    return oldCarsDetails;
}



module.exports = problem5;