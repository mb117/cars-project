let problem5 = require('../problem5.js')
let inventory = require('../cars.js');
let problem4 = require('../problem4.js');

// get all the car dates in the inventory
let dates = problem4(inventory);

let result = problem5(inventory, dates,2000);

console.log(`The length of the array returned is ${result.length}`);