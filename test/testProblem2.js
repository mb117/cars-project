let problem2 = require('../problem2.js');
let inventory = require('../cars.js');

let result = problem2(inventory);

console.log(`Last car is a ${result[0]} ${result[1]}`);