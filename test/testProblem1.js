let problem1 = require('../problem1.js')
let inventory = require('../cars.js');

let result = problem1(inventory,33);
console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);