// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


/**
 * Sort car model names of the inventory in alphabetical order
 * @param  {Object[]} inventory Inventory of cars in the dealership
 * @return {Array} model names in alphabetical order 
 */
 function problem3(inventory) {

    // return empty object if empty or invalid inventory data type 
    if (  !Array.isArray(inventory) || inventory.length === 0) {
        return [];
    }

    let carNames = [];

    for ( let car of inventory) {
        // car should be an object
        if ( typeof car !== 'object') {
            return [];
        }
        let carName = car.car_model;
        carNames.push(carName);
    }

    carNames.sort((a,b) => {
        // converting both to uppercase to ignore case when comparing
        if ( a.toUpperCase() < b.toUpperCase()) {
            // sort a before b
            return -1;
        } else if ( a.toUpperCase() > b.toUpperCase()) {
            // sort b before a
            return 1;
        } else {
            // preserve the order
            return 0;
        }
    });
    return carNames;
}


module.exports = problem3;