// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


/**
 * Gets the details of the car with a specific ID
 * @param  {Object[]} inventory Inventory of cars in the dealership
 * @return {Array} all the car years
 */
 function problem4(inventory) {
    // return empty object if empty or invalid inventory data type 
    if (  !Array.isArray(inventory) || inventory.length === 0) {
        return [];
    }

    let carYears = [];

    for ( let car of inventory) {
        // car should be an object
        if ( typeof car !== 'object') {
            return [];
        }
        let carYear = car.car_year;
        carYears.push(carYear);
    }
    return carYears;
}


module.exports = problem4;